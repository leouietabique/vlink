import React, { useEffect, useState } from 'react'
import { useQuery } from '@apollo/client';
import gql from 'graphql-tag'
import { useNavigate } from 'react-router-dom';

const GET_LINK = gql`
    query link($where: link_bool_exp) { 
        data: link(where:$where, limit: 1) {
            created_at
            expire_at
            hashed
            id
            is_premium
            link
            updated_at
            user_id
        }
    }  
`

const index = () => {
    const navigate = useNavigate();

    useQuery(GET_LINK, {
        fetchPolicy: 'no-cache',
        variables: {
            where: {
                hashed: {
                    _eq: window.location.pathname.slice(1)
                }
            }
        },
        onCompleted(res) {
            if(res.data?.length) {
                window.location.replace(res?.data[0].link)
            }
        }
    })

    return (
        <div>index 123</div>
    )
}

export default index