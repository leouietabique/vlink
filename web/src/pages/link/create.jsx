import React from 'react'
import Button from '@/components/common/Button'
import Icon from '@/components/common/Icon'

const create = () => {
    return (
        <div className='h-[80vh] w-full flex justify-center items-center'>
            <div className="flex">
                <input type="text" name="price" id="price" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500" placeholder="Paste link here ..." required="" />
                <Button
                btnClass=''
                icon={<Icon />}
                />
            </div>
        </div>
    )
}

export default create