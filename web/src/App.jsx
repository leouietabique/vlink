import React, { useEffect } from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import Layout from './pages/layout'
import Home from './pages/home'
import Documentation from './pages/docs'
import CreateLink from './pages/link/create'
import Login from './pages/authentication/login';

const client = new ApolloClient({
    uri: 'https://vlink-v2.hasura.app/v1/graphql',
    headers: {
        'x-hasura-admin-secret': 'izzdISSxa0rx8Yv8gpXEYuB5Yf6Ch6y8TXiEta138nTNhE53u4d21G1cDTZP7mHl'
    },
    cache: new InMemoryCache(),
});

const App = () => {

    return (
        <ApolloProvider client={client}>
                <BrowserRouter>
                    <Routes>
                        <Route element={<Layout />}>
                            <Route path='/' element={<Home />} />
                            <Route path='/create-link' exact element={<CreateLink />} />
                            <Route path='/documentation' element={<Documentation />} />
                        </Route>
                        <Route path='/login' element={<Login />} />
                    </Routes>
                </BrowserRouter>
        </ApolloProvider>
    )
}

export default App