import React, { useEffect, useState } from 'react'

const useTheme = () => {
    const root = document.documentElement
    const [theme, setTheme] = useState('dark')

    useEffect(() => {
        // root.classList.add('dark')
    }, [])

    const toggleTheme = () => {
        const localTheme = localStorage.getItem('theme')
        if(localTheme==='dark') {
            root.classList.add('dark')
            localStorage.setItem('theme', localTheme)
        }else {
            root.classList.remove('dark')
            localStorage.setItem('theme', localTheme)
        }
    }

    // useEffect(() => {
    //     if(theme=='dark') {
    //         root.classList.add('dark')
    //         localStorage.setItem('theme', theme)
    //     }else {
    //         root.classList.remove('dark')
    //         localStorage.setItem('theme', theme)
    //     }
    // }, [theme])

    return {
        toggleTheme
    }

}

export default useTheme