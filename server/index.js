const express = require("express");
const cors = require("cors");
const jwt = require("jsonwebtoken");
const app = express();

app.use(cors());

app.get('/hello', (req, res) => {
    const token = jwt.sign('secret', 'shhhh')
    res.send({ token })
})

app.listen(3016, () => {
    console.log('App listening at http://localhost:3016');
})